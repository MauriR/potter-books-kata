const BOOK_PRICE = 8

describe('Kata Potter easy part', () => {
  it('charges an specified prize for a copy of any of the existing 5 books', () => {
    stoneCopies = 1


    const purchase = bookSeller(stoneCopies)

    expect(purchase).toBe(BOOK_PRICE)
  })

  it('offers 5% discount on buying 2 different books', () => {
    stoneCopies = 1
    chamberCopies = 1
    const rawPrice = BOOK_PRICE * (stoneCopies + chamberCopies)
    const discountInPercent = 0.05
    const discountInMoney = rawPrice * discountInPercent

    const purchase = bookSeller2(stoneCopies, chamberCopies)


    expect(purchase).toBe(rawPrice - discountInMoney)

  })
  it('offers 10% discount on buying 3 different books', () => {
    stoneCopies = 1
    chamberCopies = 1
    prisonerCopies = 1
    const rawPrice = BOOK_PRICE * (stoneCopies + chamberCopies + prisonerCopies)
    const discountInPercent = 0.10
    const discountInMoney = rawPrice * discountInPercent

    const purchase = bookSeller2(stoneCopies, chamberCopies, prisonerCopies)

    expect(purchase).toBe(rawPrice - discountInMoney)

  })

  it('offers 20% discount on buying 4 different books', () => {
    stoneCopies = 1
    chamberCopies = 1
    prisonerCopies = 1
    golbetCopies = 1

    const rawPrice = BOOK_PRICE * (stoneCopies + chamberCopies + prisonerCopies + golbetCopies)
    const discountInPercent = 0.20
    const discountInMoney = rawPrice * discountInPercent

    const purchase = bookSeller2(stoneCopies, chamberCopies, prisonerCopies, golbetCopies)

    expect(purchase).toBe(rawPrice - discountInMoney)
  })

  it('offers 25% discount on buying the 5 different books', () => {
    const fiveBooks = 5
    const rawPrice = BOOK_PRICE * fiveBooks
    const discountInPercent = 0.25
    const discountInMoney = rawPrice * discountInPercent

    const purchase = bookSeller(fiveBooks, 0)

    expect(purchase).toBe(rawPrice - discountInMoney)

  })
})

describe('Kata Potter Hard Part', () => {
  it('Allows to buy the same book twice without taking part in the discount', () => {

    const differentBooks = 2
    const repeatedBooks = 1
    const priceWithOffer = 15.2
    const priceWithoutOffer = 8 * repeatedBooks

    const purchase = bookSeller(differentBooks, repeatedBooks)

    expect(purchase).toBe(priceWithOffer + priceWithoutOffer)


  })

  it('calculates the best discount on a given shopping basket', () => {
    stoneCopies = 2
    chamberCopies = 2
    prisonerCopies = 2
    golbetCopies = 1
    orderCopies = 1

    final = 51.60

    const purchase = bookSeller2(stoneCopies, chamberCopies, prisonerCopies, golbetCopies, orderCopies)

    expect(purchase).toBe(final)
  })

  it('offers 40% discount on buying 6 different books', () => {
    const stoneCopies = 1
    const chamberCopies = 1
    const prisonerCopies = 1
    const golbetCopies = 1
    const orderCopies = 1
    const princeCopies = 1

    const rawPrice = BOOK_PRICE * (stoneCopies + chamberCopies + prisonerCopies + golbetCopies + orderCopies + princeCopies)
    const discountInPercent = 0.40
    const discountInMoney = rawPrice * discountInPercent

    const purchase = bookSeller2(stoneCopies, chamberCopies, prisonerCopies, golbetCopies, orderCopies, princeCopies)


    expect(purchase).toBe(rawPrice - discountInMoney)
  })
})


function bookSeller(diferentBooks, repeatedBooks) {

  if (!repeatedBooks) {
    repeatedBooks = 0
  }
  let singleUnitPrice = 8
  let priceBeforeDiscount = diferentBooks * singleUnitPrice
  let discountPrice = priceBeforeDiscount * discount[diferentBooks]

  let regularPrice = repeatedBooks * singleUnitPrice

  return discountPrice + regularPrice
}


function bookSeller2(...bookList) {
  let shoppingBasket = []
  let numberOfLoops = _calculateLoop(bookList)
  let sortedCopies = 0
  
  while (_atLeastOneLoop(numberOfLoops)) {
    const differentBooksPack = sortPerDifferentUnits(bookList, sortedCopies)
    const pricePerPack = BOOK_PRICE * differentBooksPack * discount[differentBooksPack]
    shoppingBasket.push(pricePerPack)
    sortedCopies++
    numberOfLoops--
  }

  return shoppingBasket.reduce((newBookGroop, accumulatedPrice) => {
    const finalPrice = newBookGroop + accumulatedPrice

    return finalPrice
  })
}

function sortPerDifferentUnits(bookList, sortedCopies){
return bookList.filter(element => element - sortedCopies > 0).length
}

function _atLeastOneLoop(loops) {

  return loops > 0
}


function _calculateLoop(array) {
  const biggestValue = Math.max(...array)

  return biggestValue
}

let discount = {
  1: 1,
  2: 0.95,
  3: 0.90,
  4: 0.80,
  5: 0.75,
  6: 0.60,
  7: 0.50,
}
